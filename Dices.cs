﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dices
{
    internal class Program
    {
        static List<Dice> HandleInput()
        {
            int n = int.Parse(Console.ReadLine());
            List<Dice> returnValue = new List<Dice>();
            for (int i = 0; i < n; i++)
            {
                string line = Console.ReadLine();
                returnValue.Add(new Dice(line.Split().Select(int.Parse).ToList()));
            }
            return returnValue;
        }

        static int GreatestSum(List<Dice> dices)
        {
            int greatestSum = 0;
            for (int i = 0; i < 6; i++)
            {
                int sum = dices[0].GetMaxSide(i);
                int currentEyesUp = dices[0].GetEyesUp(i);
                foreach (var dice in dices.Skip(1))
                {
                    sum += dice.GetMaxSide(currentEyesUp);
                    currentEyesUp = dice.GetEyesUp(currentEyesUp);
                }
                greatestSum = Math.Max(sum, greatestSum);
            }
            return greatestSum;
        }

        public static void Main(string[] args)
        {
            List<Dice> dices = HandleInput();
            Console.WriteLine(GreatestSum(dices));
        }
    }
    class Dice
    {
        private readonly List<int> _sides;

        public Dice(List<int> sides)
        {
            _sides = sides;
        }

        public int GetEyesUp(int eyesDown)
        {
            int oppositeSite;
            switch (_sides.IndexOf(eyesDown))
            {
                case 0: oppositeSite = 5;
                    break;
                case 1: oppositeSite = 3;
                    break;
                case 2: oppositeSite = 4;
                    break;
                case 3: oppositeSite = 1;
                    break;
                case 4: oppositeSite = 2;
                    break;
                default: oppositeSite = 0;
                    break;
            }
            return _sides[oppositeSite];
        }

        public int GetMaxSide(int eyesDown) => _sides.Where(x => x != eyesDown && x != GetEyesUp(eyesDown)).Max();
    }
}