def mVal(dice, v):
    ns = {0:[5,0],1:[3,1],2:[4,2],3:[1,3],4:[2,4],5:[0,5]}[dice.index(v)]
    return max(y for x,y in enumerate(dice) if not x in ns), dice[ns[0]]
def bestValsForSide(dices):
    for x in dices[0]:
        s, nextV = mVal(dices[0], x)
        for y in dices[1:]:
            add, nextV = mVal(y, nextV)
            s += add
        yield s
d = []
for x in range(int(input())):d.append(list(map(int,input().split())))
print(max(bestValsForSide(d)))
